package connection.factorys;

import connection.factorys.ConexionAbstractFactory;
import connection.services.ConexionWSComprasServicesImpl;
import connection.services.ConexionWSVentasServicesImpl;
import connection.services.IConexionServices;

public class ConexionWSFactory extends ConexionAbstractFactory{
	@Override
	public IConexionServices getConexion(String cnxMotor) {
		//Se hace uso de los patrones: Fabrica y Singleton
		switch(cnxMotor) {
			case "COMPRAS": return new ConexionWSComprasServicesImpl();
			case "VENTAS": return new ConexionWSVentasServicesImpl();			
		
		}
		return null;
	}
}
