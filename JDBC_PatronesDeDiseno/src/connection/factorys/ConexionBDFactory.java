package connection.factorys;

import connection.factorys.ConexionAbstractFactory;
import connection.services.ConexionBDMysqlServicesImpl;
import connection.services.ConexionBDOracleServicesImpl;
import connection.services.ConexionBDVaciaServicesImpl;
import connection.services.IConexionServices;

public class ConexionBDFactory extends ConexionAbstractFactory{
	@Override
	public IConexionServices getConexion(String shapeType) {
		if(shapeType==null) {
			return ConexionBDVaciaServicesImpl.getInstancia();
		}
		//Se hace uso de los patrones: Fabrica y Singleton
		switch(shapeType) {
			case "ORACLE": return ConexionBDOracleServicesImpl.getInstancia();
			case "MYSQL": return ConexionBDMysqlServicesImpl.getInstancia();
		}
		return ConexionBDVaciaServicesImpl.getInstancia();
	}	
}
