package connection.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
 
public class ConexionBDOracleServicesImpl implements IConexionServices{
	
	//Declaracion de instancia
	private static ConexionBDOracleServicesImpl instancia;
	
	//Para mantener solo una conexion
	private Connection cnx; 
	
	//Para evitar instancia mediante operador NEW
	private ConexionBDOracleServicesImpl () {
	    String DRIVER = "oracle.jdbc.OracleDriver";
	    try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
	}
	//Obtiene una unica instancia por este metodo
	//static hace posible esto
	//PATRON: SINGLETON
	public static ConexionBDOracleServicesImpl getInstancia() {
		if(instancia ==null) {
			instancia = new ConexionBDOracleServicesImpl();		
		}
		return instancia;
	}
	public void conectar() {
		String password = "CURSOJAVA";
		String usuario = "CURSOJAVA";
		String url = "jdbc:oracle:thin:@//127.0.0.1:1521/xe";
		//try-catch-with-resources
		try {			
			if (cnx == null || cnx.isClosed()) {
				cnx = DriverManager.getConnection(url,usuario,password);
				cnx.setAutoCommit(false);
				System.out.println("Conectado..");
			}
		}catch (SQLException e) {
			System.out.println("No se pudo conectar a la base de datos");
			e.printStackTrace();
		}
	}
	
	public void desconectar() {
		try {
			if(cnx !=null) {
				cnx.close();
				System.out.println("DesConectado..");
			}else {
				System.out.println("No se pudo desconectar a la base de datos: NULL");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public String leerURL(String url) {
		// TODO Auto-generated method stub
		return null;
	}
}