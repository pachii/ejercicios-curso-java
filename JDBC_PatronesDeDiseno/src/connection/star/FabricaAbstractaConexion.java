package connection.star;

import connection.factorys.ConexionAbstractFactory;
import connection.factorys.ConexionProducerFactory;
import connection.services.IConexionServices;

public class FabricaAbstractaConexion {
	public static void main(String[] args) {
		//Se hace uso de ABSTRACTFACTORY
		//Fabrica de BASE DE DATOS
		ConexionAbstractFactory fabricaIConexion = ConexionProducerFactory.getFactory("BD");
		IConexionServices cnxBdOracle=fabricaIConexion.getConexion("ORACLE");
		cnxBdOracle.conectar();
		
		//Fabrica de WEB SERVICES
		fabricaIConexion = ConexionProducerFactory.getFactory("WS");
		IConexionServices cnxWSCompras=fabricaIConexion.getConexion("COMPRAS");
		cnxWSCompras.conectar();
		
	}
}
