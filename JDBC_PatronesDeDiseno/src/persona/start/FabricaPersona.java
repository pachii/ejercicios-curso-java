package persona.start;

import java.sql.SQLException;

import persona.bean.Persona;
import persona.dao.IPersonaDAO;

public class FabricaPersona {
	public static void main(String[] args) {
		//Se hace uso de FACTORY
		IPersonaDAO personaAutoCommit = IPersonaDAO.newInstance(true);
		try {
			System.out.println("SELECT...");
			personaAutoCommit.selectAll().forEach(persona -> System.out.println(persona.toString()));
			System.out.println("\n");
			System.out.println("INSERT...");
			personaAutoCommit.insert(new Persona("Pachi ++","LOpezzzzss",23));
			System.out.println("\n");
			System.out.println("SELECT...");
			personaAutoCommit.selectAll().forEach(persona -> System.out.println(persona.toString()));
			System.out.println("\n");
			System.out.println("UPDATE...");
			personaAutoCommit.update(new Persona(1,"pACHI","",90));			
			System.out.println("\n");
			System.out.println("SELECT...");
			personaAutoCommit.selectAll().forEach(persona -> System.out.println(persona.toString()));
			System.out.println("\n");
			System.out.println("DELETE...");
			personaAutoCommit.delete(1);
			System.out.println("\n");
			System.out.println("SELECT...");
			personaAutoCommit.selectAll().forEach(persona -> System.out.println(persona.toString()));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
