package persona.dao;

import java.sql.SQLException;
import java.util.List;

import persona.bean.Persona;

public interface IPersonaDAO {
    String DRIVER = "oracle.jdbc.OracleDriver";
    String PASSWORD = "CURSOJAVA";
	String USUARIO = "CURSOJAVA";
	String URL = "jdbc:oracle:thin:@//127.0.0.1:1521/xe";
	String QUERY_SELECT_PERSONAS = "select * from persona";
    String QUERY_INSERT_PERSONA_STM = "INSERT INTO PERSONA values (?,?,?,?)";
    String QUERY_DELETE_PERSONA_STM = "delete from persona where id = ?";
    String QUERY_UPDATE_PERSONA_STM = "update persona set nombre =? where id=?";
	List<Persona> selectAll() throws SQLException;
    void insert(Persona persona) throws SQLException;
    void insertAll(List<Persona> ersonas) throws SQLException;
    void update(Persona ersona) throws SQLException;
    void updateAll(List<Persona> personas) throws SQLException;
    void delete(long id) throws SQLException;
    void deleteAll(List<Long> ids) throws SQLException;

    public static IPersonaDAO newInstance(Boolean isTransaccional){
    	return isTransaccional ? new PersonaDaoImpl():new PersonaDaoImplTransaccional();       
    }
}