package persona.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.factorys.ConexionAbstractFactory;
import connection.factorys.ConexionProducerFactory;
import connection.services.IConexionServices;
import persona.bean.Persona;

public class PersonaDaoImpl implements IPersonaDAO {
	private static final String QUERY_SELECT = null;
	ConexionAbstractFactory fabricaIConexion = ConexionProducerFactory.getFactory("BD");
	IConexionServices cnxBdOracle=fabricaIConexion.getConexion("ORACLE");
	public PersonaDaoImpl() {
		try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
	}
	@Override
	public List<Persona> selectAll() throws SQLException {
		List<Persona> personas = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL,USUARIO,PASSWORD)) {
        	
        	try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SELECT_PERSONAS)){
        		ResultSet resultSet = preparedStatement.executeQuery();
	            while (resultSet.next()) {
	            	personas.add(new Persona(resultSet.getInt(1), resultSet.getString(2),resultSet.getString(3), resultSet.getInt(4)));
	            }
	            return personas;
        	} catch (SQLException ex) {
                connection.rollback();
                throw ex;
            }                
        } catch (SQLException e) {        
            throw e;
        }
	}

	@Override
	public void insert(Persona persona) throws SQLException {
		// TODO Auto-generated method stub
		try (Connection connection = DriverManager.getConnection(URL,USUARIO,PASSWORD)) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT_PERSONA_STM)){
				preparedStatement.setInt(1, 33);
				preparedStatement.setString(2, persona.getNombre());
				preparedStatement.setString(3, persona.getApellidos());
				preparedStatement.setInt(4, persona.getEdad());
				if (preparedStatement.executeUpdate() < 1) {
                    System.out.println("No se inserto el registro.");
                }
			} catch (SQLException ex) {
                connection.rollback();
                throw ex;
            }            
        } catch (SQLException e) {        
            throw e;
        }
		
	}

	@Override
	public void insertAll(List<Persona> ersonas) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Persona persona) throws SQLException {
		// TODO Auto-generated method stub
		try (Connection connection = DriverManager.getConnection(URL,USUARIO,PASSWORD)) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE_PERSONA_STM)){
				preparedStatement.setString(1, persona.getNombre());
				preparedStatement.setInt(2, persona.getId());
				if (preparedStatement.executeUpdate() < 1) {
                    System.out.println("No se ACTUALIZO el registro.");
                }
			} catch (SQLException ex) {
                connection.rollback();
                throw ex;
            }            
        } catch (SQLException e) {        
            throw e;
        }
		
	}

	@Override
	public void updateAll(List<Persona> personas) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(long id) throws SQLException {
		// TODO Auto-generated method stub
		try (Connection connection = DriverManager.getConnection(URL,USUARIO,PASSWORD)) {
			try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_DELETE_PERSONA_STM)){
				preparedStatement.setLong(1, id);
				if (preparedStatement.executeUpdate() < 1) {
                    System.out.println("No se ACTUALIZO el registro.");
                }
			} catch (SQLException ex) {
                connection.rollback();
                throw ex;
            }            
        } catch (SQLException e) {        
            throw e;
        }
		
	}

	@Override
	public void deleteAll(List<Long> ids) throws SQLException {
		// TODO Auto-generated method stub
		
	}


}