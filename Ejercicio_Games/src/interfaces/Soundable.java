package interfaces;

public interface Soundable {
	void playMusic(String song);
	default String setGameName(String name) {
		return name;
	}
}
