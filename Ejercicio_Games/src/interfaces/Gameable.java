package interfaces;

public interface Gameable {
	void startGame();
	default String setGameName(String name) {
		return name;
	}
}
