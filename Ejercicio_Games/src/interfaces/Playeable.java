package interfaces;

public interface Playeable {

	abstract void walk(double x, double y);		
	default String setGameName(String name) {
		return name;
	}
}
