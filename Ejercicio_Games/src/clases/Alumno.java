package clases;

public class Alumno {
	private String nombre;
	private String apellidoMaterno;
	private String apellidoPaterno;
	private String nombreCurso;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getNombreCurso() {
		return nombreCurso;
	}
	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}
	@Override
	public String toString() {
		return "Alumno [nombre=" + nombre + ", apellidoMaterno=" + apellidoMaterno + ", apellidoPaterno="
				+ apellidoPaterno + ", nombreCurso=" + nombreCurso + "]";
	}
	
}
