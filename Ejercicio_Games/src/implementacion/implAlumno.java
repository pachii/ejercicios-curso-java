package implementacion;


import java.util.ArrayList;
import java.util.List;

import clases.Alumno;
import java.util.stream.Collectors;

public class implAlumno {
	
	public static void main(String[] args) {
		
		String nombres[]= {"Francis","Jorge","Juan","Luis","Gerardo",
							"Francisco","Jorgy","Juana","Luisa","Gustavo",
							"Frank","Jorg","Jua","suisa","Gusavo",
							"Fraile","Juanita","Juana","Luisito","Gustava"};
		
		String apellidos[]= {"Lopez","Luca","Lazaro","Marquez","Casanova",
							"Ramirez","Guerrero","Juarez","Luisa","Martinez",
							"Porter","Jorg","Jua","suisa","lopez",
							"Fraile","Perez","Franson","Lucasone","Luna"};

		
		List<Alumno> listAl=new ArrayList<Alumno>();
		for(int x=0; x<20;x++) {
			Alumno al= new Alumno();
			al.setNombre(nombres[x]);
			al.setApellidoPaterno(apellidos[x]);
			al.setApellidoMaterno(apellidos[(apellidos.length-1)-x]);
			al.setNombreCurso("CursoJava");
			listAl.add(al);	
			al=null;
			
		}
		System.out.println("\nTODOS LOS ALUMNOS\n");
		//Todos los alumnos
		listAl.stream()
		.forEach(alumno->System.out.println(alumno.toString()));
		//Cantidad de alumnos
		System.out.println("\nCANTIDAD DE ALUMNOS: "+listAl.stream().count()+"\n");
		
		//Alumnos que su nombre termina con A
		System.out.println("\nALUMNOS QUE SU NOMBRE TERMINE CON A\n");		
		listAl.stream()
		.filter(alumno->alumno.getNombre().endsWith("a"))
		.forEach(alumno->System.out.println(alumno.toString()));
		
		//Los primero 5 alumnos..
		System.out.println("\nLOS PRIMERO 5 ALUMNOS\n");
		listAl.stream()
		.limit(5)
		.forEach(alumno->System.out.println(alumno.toString()));
		


	}
	

}
