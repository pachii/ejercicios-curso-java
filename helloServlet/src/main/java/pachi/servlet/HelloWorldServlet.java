package pachi.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldServlet extends HttpServlet{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4751096228274971485L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		//response.getWriter().println("Hello World!");
		
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		out.println("Hello World! PACHI++");
		out.println("</body></html>");
		out.close();
	}
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// selecciona la cabecera de tipo de contenido antes de acceder a Writer
		response.setContentType("text/html; charset=UTF-8");
		
		try{
			BufferedReader reader= request.getReader();
			String line;
			while((line = reader.readLine())!=null) {
				System.out.println(line);
			}
			PrintWriter out = response.getWriter();

			out.println("<html>");
			out.println("<body>");
			out.println("<t1>" + "PACHIIII" + "</t1>");
			out.println("</body>");
			out.println("</html>");
			//PrintWriter out = response.getWriter();
			// Luego escribe la respuesta
			/*out.println("<html>" +
			         "<head><title> Receipt </title>");
			out.println("<h3>Thank you for purchasing your books from us " +
	        request.getParameter("cardname"));
			out.close();*/	
		}catch(IOException e) {
			e.printStackTrace();			
		}		
	}


}
