package pachi.servlet;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String user = req.getParameter("user");
		String pass = req.getParameter("password");
		if ("pachi".equals(user) && "pachi".equals(pass)) {
			response(resp, "login ok");
		} else {
			response(resp, "invalid login");
		}
	}

	private void response(HttpServletResponse resp, String msg) throws IOException {
		PrintWriter out = resp.getWriter();
		out.println("<html>");
		out.println("<body>");
		out.println("<t1>" + msg + "</t1>");
		out.println("</body>");
		out.println("</html>");
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//String static final String HEADER="<html><head><title>%s</title><meta http-equiv=\"";
		String user = req.getParameter("user");
		String pass = req.getParameter("password");
		String[] tech = req.getParameterValues("tecnologias");
		StringBuilder body = new StringBuilder("<h1>Procesando Parametros</h1>");
		//body.append(b)
		
		if ("pachi".equals(user) && "pachi".equals(pass)) {
			response(resp, "login ok metodo POST................");
		} else {
			response(resp, "invalid login metodo POST..........");
		}		
	}
}