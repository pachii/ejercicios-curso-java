--------------------------------------------------------
-- Archivo creado  - viernes-abril-17-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table LOG
--------------------------------------------------------

  CREATE TABLE "WLMXNGEN"."LOG" 
   (	"LOGGER" VARCHAR2(2000 BYTE), 
	"LEVELL" VARCHAR2(2000 BYTE), 
	"MESSAGE" VARCHAR2(2000 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SICE_SPEI_DATA_WLMXNGEN" ;
--------------------------------------------------------
--  DDL for Table PERSONA
--------------------------------------------------------

  CREATE TABLE "WLMXNGEN"."PERSONA" 
   (	"ID" NUMBER, 
	"NOMBRE" VARCHAR2(20 BYTE), 
	"APELLIDOS" VARCHAR2(20 BYTE), 
	"EDAD" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SICE_SPEI_DATA_WLMXNGEN" ;
REM INSERTING into WLMXNGEN.LOG
SET DEFINE OFF;
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','TRACE','========Test Trace');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','DEBUG','Test Debug');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','INFO','Test info');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','WARN','Test warn');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','ERROR','Test error');
Insert into WLMXNGEN.LOG (LOGGER,LEVELL,MESSAGE) values ('testLog4jJDBC.testLog4j','FATAL','Test fatal==========');
REM INSERTING into WLMXNGEN.PERSONA
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index PERSONA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "WLMXNGEN"."PERSONA_PK" ON "WLMXNGEN"."PERSONA" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SICE_SPEI_DATA_WLMXNGEN" ;
--------------------------------------------------------
--  Constraints for Table PERSONA
--------------------------------------------------------

  ALTER TABLE "WLMXNGEN"."PERSONA" ADD CONSTRAINT "PERSONA_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SICE_SPEI_DATA_WLMXNGEN"  ENABLE;
  ALTER TABLE "WLMXNGEN"."PERSONA" MODIFY ("ID" NOT NULL ENABLE);
