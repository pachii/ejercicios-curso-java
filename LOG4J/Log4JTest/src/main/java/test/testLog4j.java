package test;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class testLog4j {

	public static void main(String[] args) throws InterruptedException{
		// TODO Auto-generated method stub
		final Logger logger=LogManager.getLogger();
		while(true) {
			logger.trace("========Test Trace");
			logger.debug("Test Debug");
			logger.info("Test info");
			logger.warn("Test warn");
			logger.error("Test error");
			logger.fatal("Test fatal==========");
			Thread.sleep(10000);
		}
		

	}

}
