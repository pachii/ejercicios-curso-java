package test;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class testLog4j2Threads  {

	public static void main(String[] args) throws InterruptedException{
		// TODO Auto-generated method stub
		final Logger logger=LogManager.getLogger();
	
		while(true) {

			Thread t1 = new Thread("Thread 1") {
				public void run() {
					logger.info("Test info");
				}
			};
			Thread t2 = new Thread("Thread 2") {
				public void run() {
					logger.info("Test Trace");
				}
			};
			Thread t3 = new Thread("Thread 3") {
				public void run() {
					logger.info("Test Debug");
				}
			};
			Thread t4 = new Thread("Thread 4") {
				public void run() {
					logger.info("Test warn");
				}
			};
			Thread t5 = new Thread("Thread 5") {
				public void run() {
					logger.info("Test error");
				}
			};
			
			Thread t6 = new Thread("Thread 6") {
				public void run() {
					logger.info("Test Fatal");
				}
			};
			
			t1.start();
			t2.start();
			t3.start();
			t4.start();
			t5.start();
			t6.start();
			Thread.sleep(10000);
		}
	}

}
