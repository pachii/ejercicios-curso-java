package test;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class testLog4j2ThreadsLamdas  {

	public static void main(String[] args) throws InterruptedException{
		// TODO Auto-generated method stub
		final Logger logger=LogManager.getLogger();
		
		Runnable r1 = () -> {
			logger.info("Test LAMDA->INFO");
		};				
		Runnable r2 = () -> {
			logger.info("Test LAMDA-> DEBUG");
		};
		Runnable r3 = () -> {
			logger.info("Test LAMDA -> TRACE");
		};
		Runnable r4 = () -> {
			logger.info("Test LAMDA -> ERROR");
		};
		
		Runnable r5 = () -> {
			logger.info("Test LAMDA -> FATAL");
		};

		Runnable r6 = () -> {
			logger.info("Test LAMDA -> WARN");
		};		
		while(true) {

			Thread t1 = new Thread(r1,"Thread 1");
			Thread t2 = new Thread(r2,"Thread 2");
			Thread t3 = new Thread(r3,"Thread 3");
			Thread t4 = new Thread(r4,"Thread 4");
			Thread t5 = new Thread(r5,"Thread 5");
			Thread t6 = new Thread(r6,"Thread 6");		
			t1.start();
			t2.start();
			t3.start();
			t4.start();
			t5.start();
			t6.start();
			Thread.sleep(10000);
		}
	}

}
