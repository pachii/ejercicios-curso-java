package connection.factorys;

public class ConexionProducerFactory {
   public static ConexionAbstractFactory getFactory(String fabrica){   
	  switch(fabrica) {
	  	case "BD": return new ConexionBDFactory();
	  	case "WS": return new ConexionWSFactory();
	  }
	return null;
   }
}