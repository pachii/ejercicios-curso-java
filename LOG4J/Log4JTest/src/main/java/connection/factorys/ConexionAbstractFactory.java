package connection.factorys;

import connection.services.IConexionServices;

public abstract  class ConexionAbstractFactory {
	public abstract IConexionServices getConexion(String shapeType);
}
