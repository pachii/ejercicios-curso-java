package connection.services;

public interface IConexionServices {
	void conectar();
	void desconectar();
	String leerURL(String url);
}
