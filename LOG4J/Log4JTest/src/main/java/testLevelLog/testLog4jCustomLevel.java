package testLevelLog;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class testLog4jCustomLevel {

	public static void main(String[] args) throws InterruptedException{
		// TODO Auto-generated method stub
		final Logger logger=LogManager.getLogger();
		
		// This creates the "VERBOSE" level if it does not exist yet.
		/*final Level VERBOSE = Level.forName("VERBOSE", 550);

		logger.log(VERBOSE, "a verbose message"); // use the custom VERBOSE level

		// Create and use a new custom level "DIAG".
		logger.log(Level.forName("DIAG", 350), "a diagnostic message");

		// Use (don't create) the "DIAG" custom level.
		// Only do this *after* the custom level is created!
		logger.log(Level.getLevel("DIAG"), "another diagnostic message");

		// Using an undefined level results in an error: Level.getLevel() returns null,
		// and logger.log(null, "message") throws an exception.
		logger.log(Level.getLevel("FORGOT_TO_DEFINE"), "some message"); // throws exception!*/
		while(true) {
			logger.log(Level.getLevel("MYLOGGER"),"Test MYLOGGER");
			logger.log(Level.getLevel("DIAG"),"Test DIAG");
			logger.log(Level.getLevel("NOTICE"),"Test NOTICE");
			logger.log(Level.getLevel("VERBOSE"),"Test VERBOSE");
			logger.trace("========Test Trace");
			logger.debug("Test Debug");
			logger.info("Test info");
			logger.warn("Test warn");
			logger.error("Test error");
			logger.fatal("Test fatal==========");
			Thread.sleep(10000);
		}
		

	}

}
