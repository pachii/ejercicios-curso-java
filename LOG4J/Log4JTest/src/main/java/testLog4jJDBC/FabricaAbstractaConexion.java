package testLog4jJDBC;

import connection.factorys.ConexionAbstractFactory;
import connection.factorys.ConexionProducerFactory;
import connection.services.IConexionServices;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class FabricaAbstractaConexion {
	/*public static void main(String[] args) {
		//Se hace uso de ABSTRACTFACTORY
		//Fabrica de BASE DE DATOS
		ConexionAbstractFactory fabricaIConexion = ConexionProducerFactory.getFactory("BD");
		IConexionServices cnxBdOracle=fabricaIConexion.getConexion("ORACLE");
		cnxBdOracle.conectar();
		
		
	}*/
	public static Connection getConnection() {
		String password = "WLMXNGEN";
		String usuario = "WLMXNGEN";
		String url = "jdbc:oracle:thin:@//127.0.0.1:1521/xe";
		Connection cnx=null;
		//try-catch-with-resources
		
		try {			
			if (cnx == null || cnx.isClosed()) {
				cnx = DriverManager.getConnection(url,usuario,password);
				cnx.setAutoCommit(false);
				System.out.println("Conectado..");
			}
		}catch (SQLException e) {
			System.out.println("No se pudo conectar a la base de datos");
			e.printStackTrace();
		}
		return cnx;
	}
}
